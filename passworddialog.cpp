#include "passworddialog.h"
#include "ui_passworddialog.h"

bool ok;

PasswordDialog::PasswordDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PasswordDialog)
{
    ui->setupUi(this);
    ok = false;
    ui->warningLabel->setStyleSheet("QLabel { color : red; }");
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
}

PasswordDialog::~PasswordDialog()
{
    delete ui;
}

void PasswordDialog::setCancelButtonText(QString text) {
    ui->cancelButton->setText(text);
}

void PasswordDialog::setTitleText(QString text) {
    ui->titleLabel->setText(text);
}

void PasswordDialog::setWarningText(QString text) {
    ui->warningLabel->setText(text);
}

void PasswordDialog::setEntryText(QString text) {
    ui->label->setText(text);
}

void PasswordDialog::lock(void) {
    ui->lineEdit->setEnabled(false);
    ui->okButton->setEnabled(false);
}

QString PasswordDialog::getPassword() {
    return ui->lineEdit->text();
}

void PasswordDialog::on_okButton_clicked()
{
    ok = true;
    close();
}

void PasswordDialog::on_cancelButton_clicked()
{
    close();
}
