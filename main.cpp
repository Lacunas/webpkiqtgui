#include <QApplication>
#include <infodialog.h>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    InfoDialog dialog;
    dialog.setMessage("Lacuna Web PKI native application.");
    dialog.show();

    return app.exec();
}
