#ifndef PASSWORDDIALOG_H
#define PASSWORDDIALOG_H

#include <QDialog>

namespace Ui {
class PasswordDialog;
}

class PasswordDialog : public QDialog
{
    Q_OBJECT

public:
    bool ok;
    explicit PasswordDialog(QWidget *parent = 0);
    ~PasswordDialog();
    void setCancelButtonText(QString text);
    void setTitleText(QString text);
    void setWarningText(QString text);
    void setEntryText(QString);
    void lock(void);
    QString getPassword(void);

private slots:
    void on_okButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::PasswordDialog *ui;
};

#endif // PASSWORDDIALOG_H
