#ifndef WEBPKI_GUI_H
#define WEBPKI_GUI_H

#include "webpki_gui_global.h"

extern "C" WEBPKI_GUISHARED_EXPORT int gui_get_password(int agrc, char **argv, void *lang, int is_pass, char &pass, int pass_maxlen, char *title, char *warning, unsigned char locked, int &ok);

extern "C" WEBPKI_GUISHARED_EXPORT int gui_authorize(int argc, char **argv, void *lang, int nsig, char *info, int &authorized, int &dontaskagain);

extern "C" WEBPKI_GUISHARED_EXPORT int gui_choose_file(int argc, char **argv, char &path, int path_maxlen, const char *title, int &ok);

extern "C" WEBPKI_GUISHARED_EXPORT int gui_choose_dir(int argc, char **argv, char &path, int path_maxlen, const char *title, int &ok);

extern "C" WEBPKI_GUISHARED_EXPORT int gui_success(int argc, char **argv, const char *message);

extern "C" WEBPKI_GUISHARED_EXPORT int gui_info(int argc, char **argv, const char *message);

extern "C" WEBPKI_GUISHARED_EXPORT int gui_about(int argc, char **argv, const char *version);

#endif // WEBPKI_GUI_H
