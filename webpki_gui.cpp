#include <QApplication>
#include <QFileDialog>
#include <string.h>
#include "webpki_gui.h"
#include "authorizedialog.h"
#include "passworddialog.h"
#include "successdialog.h"
#include "infodialog.h"
#include "aboutdialog.h"

typedef struct {
    const char *OK;
    const char *Cancel;
    const char *Import;
    const char *Select;
    const char *Pin;
    const char *PinTitle;
    const char *PinIncorrect;
    const char *PinLastTry;
    const char *PinLocked;
    const char *PinLockedTitle;
    const char *PinListTitle;
    const char *PinSignTitle;
    const char *Password;
    const char *PasswordTitle;
    const char *PasswordIncorrect;
    const char *Allow;
    const char *AuthorizeTitle;
    const char *AuthorizeTitleMulti;
    const char *AuthorizeColons;
    const char *AuthorizeColonsMulti;
    const char *DontAskAgain;
    const char *ImportSuccessful;
    const char *ListSuccessful;
    const char *P12ImportColons;
    const char *P12ImportTitle;
    const char *SelectedDir;
    const char *OperationCanceled;
}msg_t;


int gui_authorize(int argc, char **argv, void *lang, int nsig, char *info, int &authorized, int &dontaskagain) {
    int result;
    msg_t *lang_msgs = (msg_t*)lang;

    authorized = 0;
    dontaskagain = 0;

    QApplication app(argc, argv);

    AuthorizeDialog dialog;
    dialog.setAuthButtonText(lang_msgs->Allow);
    dialog.setCancelButtonText(lang_msgs->Cancel);
    if (nsig > 1) {
        dialog.setTitleText(lang_msgs->AuthorizeTitleMulti);
        dialog.setContentText(lang_msgs->AuthorizeColonsMulti);
    } else {
        dialog.setTitleText(lang_msgs->AuthorizeTitle);
        dialog.setContentText(lang_msgs->AuthorizeColons);
    }
    dialog.setInfoText(info);
    dialog.setCheckBoxText(lang_msgs->DontAskAgain);
    dialog.show();
    QApplication::alert(&dialog);

    result = app.exec();

    if (dialog.authorized) {
        authorized = 1;
    }

    if (dialog.dontAskAgain()) {
        dontaskagain = 1;
    }

    return result;
}

int gui_get_password(int argc, char **argv, void *lang, int is_pass, char &pass, int pass_maxlen, char *title, char *warning, unsigned char locked, int &ok) {
    int result;
    msg_t *lang_msgs = (msg_t*)lang;
    ok = 0;

    QApplication app(argc, argv);

    PasswordDialog dialog;
    dialog.setCancelButtonText(lang_msgs->Cancel);
    if (is_pass) {
        dialog.setEntryText("Password:");
    }
    dialog.setTitleText(title);
    dialog.setWarningText(warning);

    if (locked) {
        dialog.lock();
    }

    dialog.show();
    QApplication::alert(&dialog);
    result = app.exec();

    if (dialog.ok) {
        ok = 1;
        QByteArray pass_data = dialog.getPassword().toLatin1();
        if (pass_data.length() < pass_maxlen) {
            strncpy(&pass, pass_data.data(), pass_data.length());
        } else {
            // TODO limit exceeded
            return -1;
        }
    }

    return result;
}

int gui_choose_file(int argc, char **argv, char &path, int path_maxlen, const char *title, int &ok) {
    ok = 0;

    QApplication app(argc, argv);
    QString filePath = QFileDialog::getOpenFileName(0, QString(title), QDir::homePath());

    if (filePath == NULL) {
        // user canceled
        return 0;
    }

    QByteArray path_data = filePath.toLatin1();
    if (path_data.length() < path_maxlen) {
        strncpy(&path, path_data.data(), path_data.length());
    } else {
        // TODO limit exceeded
        return -1;
    }
    ok = 1;
    return 0;
}

int gui_choose_dir(int argc, char **argv, char &path, int path_maxlen, const char *title, int &ok) {
    ok = 0;

    QApplication app(argc, argv);
    QString dirPath = QFileDialog::getExistingDirectory(0, QString(title), QDir::homePath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if (dirPath == NULL) {
        // user canceled
        return 0;
    }
    QByteArray path_data = dirPath.toLatin1();
    if (path_data.length() < path_maxlen) {
        strncpy(&path, path_data.data(), path_data.length());
    } else {
        // TODO limit exceeded
        return -1;
    }
    ok = 1;
    return 0;
}

int gui_success(int argc, char **argv, const char *message) {
    QApplication app(argc, argv);
    SuccessDialog dialog;
    dialog.setMessage(message);
    dialog.show();
    return app.exec();
}

int gui_info(int argc, char **argv, const char *message) {
    QApplication app(argc, argv);
    InfoDialog dialog;
    dialog.setMessage(message);
    dialog.show();
    return app.exec();
}

int gui_about(int argc, char **argv, const char *version) {
    QApplication app(argc, argv);
    AboutDialog dialog;
    dialog.setAboutInfo(version);
    dialog.show();
    return app.exec();
}

