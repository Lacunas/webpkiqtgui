#include "authorizedialog.h"
#include "ui_authorizedialog.h"

bool authorized;

AuthorizeDialog::AuthorizeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthorizeDialog)
{
    ui->setupUi(this);
    authorized = false;
    //ui->titleLabel->setStyleSheet("QLabel { background-color : red; color : blue; }");
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
}

AuthorizeDialog::~AuthorizeDialog()
{
    delete ui;
}

void AuthorizeDialog::setAuthButtonText(QString text) {
    ui->authButton->setText(text);
}

void AuthorizeDialog::setCancelButtonText(QString text) {
    ui->cancelButton->setText(text);
}

void AuthorizeDialog::setTitleText(QString text) {
    ui->titleLabel->setText(text);
}

void AuthorizeDialog::setContentText(QString text) {
    ui->contentLabel->setText(text);
}

void AuthorizeDialog::setInfoText(QString text) {
    ui->infoLabel->setText(text);
}

void AuthorizeDialog::setCheckBoxText   (QString text) {
    ui->checkBox->setText(text);
}

void AuthorizeDialog::on_authButton_clicked()
{
    authorized = true;
    close();
}

void AuthorizeDialog::on_cancelButton_clicked()
{
    close();
}

bool AuthorizeDialog::dontAskAgain() {
    return ui->checkBox->checkState() == Qt::Checked;
}
