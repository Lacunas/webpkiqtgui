#-------------------------------------------------
#
# Project created by QtCreator 2015-11-16T17:13:05
#
#-------------------------------------------------

QMAKE_MAC_SDK = macosx10.11

QT       += core widgets

TARGET = webpki-gui
TEMPLATE = lib
#TEMPLATE = app
#CONFIG += staticlib
#CONFIG += console
#CONFIG += app_bundle

DEFINES += WEBPKI_GUI_LIBRARY

SOURCES += webpki_gui.cpp \
    authorizedialog.cpp \
    passworddialog.cpp \
    successdialog.cpp \
    infodialog.cpp \
    main.cpp \
    aboutdialog.cpp

HEADERS += webpki_gui.h\
        webpki_gui_global.h \
    authorizedialog.h \
    passworddialog.h \
    successdialog.h \
    infodialog.h \
    aboutdialog.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

FORMS += \
    authorizedialog.ui \
    passworddialog.ui \
    successdialog.ui \
    infodialog.ui \
    aboutdialog.ui

RESOURCES += \
    res.qrc
