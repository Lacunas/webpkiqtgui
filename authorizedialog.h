#ifndef AUTHORIZEDIALOG_H
#define AUTHORIZEDIALOG_H

#include <QDialog>

namespace Ui {
class AuthorizeDialog;
}

class AuthorizeDialog : public QDialog
{
    Q_OBJECT

public:
    bool authorized;
    explicit AuthorizeDialog(QWidget *parent = 0);
    ~AuthorizeDialog();
    void setAuthButtonText(QString text);
    void setCancelButtonText(QString text);
    void setTitleText(QString text);
    void setContentText(QString text);
    void setInfoText(QString text);
    void setCheckBoxText(QString text);
    bool dontAskAgain();

private slots:

    void on_authButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::AuthorizeDialog *ui;
};

#endif // AUTHORIZEDIALOG_H
