#include "successdialog.h"
#include "ui_successdialog.h"

SuccessDialog::SuccessDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SuccessDialog)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
}

SuccessDialog::~SuccessDialog()
{
    delete ui;
}

void SuccessDialog::on_okButton_clicked()
{
    close();
}

void SuccessDialog::setMessage(QString text) {
    ui->messageLabel->setText(text);
}
